package mx.tecnm.misantla.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.tecnm.misantla.myapplication.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnEnviar.setOnClickListener {

             val nombre = binding.EdtName.text.toString()

            binding.tvItsav.text = "${nombre} TecNM Campus Alvarado"
        }


    }
}

